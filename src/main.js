// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// bootstrap vue
import bootstrapVuePlugin from './plugins/bootstrapVuePlugin'
// toast notification
import toastPlugin from './plugins/toastPlugin'
// axios
import VueAxios from 'vue-axios'
import axiosPlugin from './plugins/axiosPlugin'
// store
import { store } from './store'
// filters
import { currency } from './filters/currency'

Vue.config.productionTip = false

// register plugin
Vue.use(toastPlugin)
Vue.use(VueAxios, axiosPlugin)
Vue.use(bootstrapVuePlugin)

// register filter
Vue.filter('currency', currency)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
