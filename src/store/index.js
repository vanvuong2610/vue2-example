import Vue from 'vue'
import Vuex from 'vuex'

// plugin local storage
import VuexPersistence from 'vuex-persist'
import logger from '@/plugins/logger'

import { user } from './user'

Vue.use(Vuex)

const vuexPersist = new VuexPersistence({
  key: 'my-app',
  storage: window.localStorage
})

const debug = process.env.NODE_ENV !== 'production'

export const store = new Vuex.Store({
  modules: {
    user
  },
  strict: debug,
  plugins: debug ? [logger(), vuexPersist.plugin] : [vuexPersist.plugin]
})
