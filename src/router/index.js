import Vue from 'vue'
import Router from 'vue-router'

// store
import { store } from '../store'

// layouts
import SampleLayout from '@/layouts/SampleLayout'

// public page
import Home from '@/modules/Home'
import Login from '@/modules/Login'

// middlewares
import auth from '@/middlewares/auth'
// import guest from '@/middlewares/guest'
import middlewarePipeline from './middlewarePipeline'

// module routes
import SomeModule from '@/modules/SomeModule/router.js'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '',
      name: 'SampleLayout',
      component: SampleLayout,
      children: [
        {
          path: '',
          name: 'Home',
          component: Home,
          meta: {
            middleware: [
              auth
            ]
          }
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    ...SomeModule
  ]
})

// Vue Router navigation guards
router.beforeEach((to, from, next) => {
  if (!to.meta.middleware) {
    return next()
  }
  const middleware = to.meta.middleware

  const context = {
    to,
    from,
    next,
    store
  }

  return middleware[0]({
    ...context,
    next: middlewarePipeline(context, middleware, 1)
  })
})

export default router
