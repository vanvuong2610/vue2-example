export default {
  login: (axios, data) => axios.post('/auth-service/oauth/token', data)
}
